'use strict';

// var MainScene = {
// 	active: true,
// 	preload: function() {
// 		this.load.image('jungleBckgnd', 'res/backgrounds/jungle.jpg');
// 	},
// 	create: function() {
// 		this.add.image(400, 225, 'jungleBckgnd');
// 		const changeSceneBtn = this.add.text(400, 225, 'Change Scene', {fontSize: 34, fontFamily: 'Arial', backgroundColor: '#F00', color: '#000'});
// 		changeSceneBtn.setInteractive();

// 		changeSceneBtn.on ('pointerup', () => {console.log('clicked'); });
// 	},
// 	update: function() {

// 	},
// 	changeScene: function(name) {

// 	}
// };

class MainScene extends Phaser.Scene {
	constructor() {
		console.log('MainScene Loading!');
	}
	preload() {
		this.load.image('jungleBckgnd', 'res/backgrounds/jungle.jpg');
	}

	create () {
		this.add.image(400, 225, 'jungleBckgnd');
		const changeSceneBtn = this.add.text(400, 225, 'Change Scene', {fontSize: 34, fontFamily: 'Arial', backgroundColor: '#F00', color: '#000'});
		changeSceneBtn.setInteractive();

		changeSceneBtn.on ('pointerup', () => {console.log('clicked'); });
	}

	update() {

	}
}