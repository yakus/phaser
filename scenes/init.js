var width = 800;
var height = 450;

var main = new MainScene();

var engineConfig = {
    type: Phaser.AUTO,
    width: parseInt(width),
    height: parseInt(height),
    scale: {
        parent: 'theGameContainer',
    },
    backgroundColor: '#36A9E1',
    scene: {
    	preload: main.preload,
    	create: main.create,
    	update: main.update,
    },
    physics: {
        default: 'arcade',
        arcade: {
            gravity: {y: 300},
            debug: false,
        }
    },
};

var game = new Phaser.Game(engineConfig);